// Define the root namespace
var JS = window.JS || {};

JS.filter = {

	activeClass: 'c-filter-list__active',

	// initial function
	init: function() {

		this.loadFilter();

	},

	loadFilter: function(){

		that = this;

		$('.c-filter-list__options li a').click(function(){
			that.toggleActive(this);
			that.filterBoxes(this);
		});

	},

	toggleActive: function(element) {
		// remove active class from all Li's
		$('.c-filter-list__options li').removeClass(this.activeClass);

		// add active class to selected item
		$(element).parent().addClass(this.activeClass);
	},

	filterBoxes: function(element) {

		var selectedClass = $(element).data('class');

		$('.block').removeClass('active');
		$('.'+selectedClass).addClass('active');

	}


};