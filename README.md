# Base Grunt / Bower / BEM SASS / Vagrant Setup #

Simple setup that uses Bower and Grunt to concat and minify SASS and JS files. Work out of the _dev folder adding your JS to the scripts folder and SASS to the style folder.

Grunt watch is setup so just calling grunt will compile the JS and create the CSS files. They will appear in the public folder so you just need to add these to your HTML.

This starter repository uses

### How do I get set up? ###

```shell
$ cd /projectdir/
$ npm install
$ bower install
$ grunt
```

### Setup Vagrant ###

This setup uses vagrant to run a LAMP server. To get started you need to install vagrant and virtualbox

* [Download virtualbox]: https://www.virtualbox.org/wiki/Downloads
* [Download vagrant]: https://www.vagrantup.com/downloads.html

Note: If you need to run multiple projects change the IP address in the Vagrantfile in the project root to stop clashes

```shell
$ vagrant up
```

The first time you run vagrant it'll take a little time to install the scotchbox but after this you'll be able to launch a server with ease.

Stop the server

```shell
$ vagrant halt
```

Database

```shell
$ Username: root
$ Password: root
$ IP address
```

If you need to use libraries from Bower edit the gruntfile.js at line 16 and reference the files in there. For example:

```shell
lib_scripts: [
      '<%= globalConfig.path_dist_bower %>jquery/dist/jquery.min.js',
    ]
```

### What is this repository for? ###

* Base Grunt, Bower, SASS, NormalizeCSS, FlexboxGrid, jQuery
* Version 0.0.2
