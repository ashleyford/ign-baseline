<div class="c-filter-list">

	<ul class="c-filter-list__options">
		<li class="c-filter-list__active"><a href="#" data-class="red_block">Red</a></li>
		<li><a href="#" data-class="blue_block">Blue</a></li>
		<li><a href="#" data-class="green_block">Green</a></li>
		<li><a href="#" data-class="orange_block">Orange</a></li>
		<li><a href="#" data-class="pink_block">Pink</a></li>
		<li><a href="#" data-class="purple_block">Purple</a></li>
		<li><a href="#" data-class="block">All</a></li>
	</ul>

	<div class="c-filter-list__items">

		<div class="row boxes">
			<div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 red_block block active">
				<div class="box-row red">Lovely Box</div>
			</div>

			<div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 orange_block block active">
				<div class="box-row orange">Lovely Box</div>
			</div>

			<div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 green_block block active">
				<div class="box-row green">Lovely Box</div>
			</div>

			<div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 red_block block active">
				<div class="box-row red">Lovely Box</div>
			</div>

			<div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 orange_block block active">
				<div class="box-row orange">Lovely Box</div>
			</div>

			<div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 green_block block active">
				<div class="box-row green">Lovely Box</div>
			</div>

			<div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 blue_block block active">
				<div class="box-row pink">Lovely Box</div>
			</div>

			<div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 orange_block block active">
				<div class="box-row orange">Lovely Box</div>
			</div>

			<div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 purple_block block active">
				<div class="box-row purple">Lovely Box</div>
			</div>

			<div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 pink_block block active">
				<div class="box-row pink">Lovely Box</div>
			</div>

			<div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 orange_block block active">
				<div class="box-row orange">Lovely Box</div>
			</div>

			<div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 green_block block active">
				<div class="box-row green">Lovely Box</div>
			</div>

			<div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 blue_block block active">
				<div class="box-row blue">Lovely Box</div>
			</div>

			<div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 pink_block block active">
				<div class="box-row pink">Lovely Box</div>
			</div>

			<div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 purple_block block active">
				<div class="box-row purple">Lovely Box</div>
			</div>

			<div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 red_block block active">
				<div class="box-row red">Lovely Box</div>
			</div>

			<div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 orange_block block active">
				<div class="box-row orange">Lovely Box</div>
			</div>

			<div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 green_block block active">
				<div class="box-row green">Lovely Box</div>
			</div>

			<div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 blue_block block active">
				<div class="box-row blue">Lovely Box</div>
			</div>

			<div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 orange_block block active">
				<div class="box-row orange">Lovely Box</div>
			</div>

			<div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 purple_block block active">
				<div class="box-row purple">Lovely Box</div>
			</div>

			<div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 red_block block active">
				<div class="box-row red">Lovely Box</div>
			</div>

			<div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 orange_block block active">
				<div class="box-row orange">Lovely Box</div>
			</div>

			<div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 green_block block active">
				<div class="box-row green">Lovely Box</div>
			</div>

			<div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 blue_block block active">
				<div class="box-row blue">Lovely Box</div>
			</div>

			<div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 orange_block block active">
				<div class="box-row orange">Lovely Box</div>
			</div>

			<div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 purple_block block active">
				<div class="box-row purple">Lovely Box</div>
			</div>

			<div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 red_block block active">
				<div class="box-row red">Lovely Box</div>
			</div>

			<div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 orange_block block active">
				<div class="box-row orange">Lovely Box</div>
			</div>

			<div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 green_block block active">
				<div class="box-row green">Lovely Box</div>
			</div>

			<div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 blue_block block active">
				<div class="box-row blue">Lovely Box</div>
			</div>

			<div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 orange_block block active">
				<div class="box-row orange">Lovely Box</div>
			</div>

			<div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 purple_block block active">
				<div class="box-row purple">Lovely Box</div>
			</div>



			<div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 blue_block block active">
				<div class="box-row blue">Lovely Box</div>
			</div>

			<div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 orange_block block active">
				<div class="box-row orange">Lovely Box</div>
			</div>

			<div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 purple_block block active">
				<div class="box-row purple">Lovely Box</div>
			</div>
		</div>

	</div>

</div>